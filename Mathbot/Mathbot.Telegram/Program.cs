﻿using System;
using System.Collections.Generic;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;

namespace Mathbot.TelegramVersion
{
    class Program
    {
        public static string botToken { get; set; } = "1975103739:AAFjgFM6A7-u_W8WefuBbHh2WID3V1px208";
        public static TelegramBotClient botClient;
        static void Main(string[] args)
        {
            botClient = new TelegramBotClient(botToken);
            botClient.StartReceiving();
            botClient.OnMessage += OnMessageHandler;
            Console.ReadLine();
            botClient.StopReceiving();
        }

        private static async void OnMessageHandler(object sender, MessageEventArgs e)
        {
            string personExample;
            string rightAnwser = Guid.NewGuid().ToString();
            var personMessage = e.Message;
            if (personMessage.Text == "/start")
            {
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, "Привет, как дела?", replyMarkup: GetPlayButtons());
            }
            else if (personMessage.Text == "Играть 🚀")
            {
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, "Окей, выбери сложность примеров.", replyMarkup: GetPlayButtons());
            }
            else if (personMessage.Text == "Список лидеров 🏆")
            {
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, "Первое место: Алексей - @alekami649\n Второе место: Кирилл - @kirillagu67");
            }
            #region Levels
            else if (personMessage.Text == "Очень легко")
            {
                personExample = Generator.randomExample(1);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 1);
            }
            else if (personMessage.Text == "Легко")
            {
                personExample = Generator.randomExample(2);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 2);
            }
            else if (personMessage.Text == "Легко+")
            {
                personExample = Generator.randomExample(3);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 3);
            }
            else if (personMessage.Text == "Нормально")
            {
                personExample = Generator.randomExample(4);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 4);
            }
            else if (personMessage.Text == "Нормально+")
            {
                personExample = Generator.randomExample(5);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 5);
            }
            else if (personMessage.Text == "Сложно")
            {
                personExample = Generator.randomExample(6);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 6);
            }
            else if (personMessage.Text == "Сложно+")
            {
                personExample = Generator.randomExample(7);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 7);
            }
            else if (personMessage.Text == "Супер сложно")
            {
                personExample = Generator.randomExample(8);
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, personExample);
                rightAnwser = Calc.GetAnwserForExample(personExample, 8);
            }
            #endregion
            #region Anwsers
            else if (personMessage.Text == rightAnwser)
            {
                botClient.SendTextMessageAsync(personMessage.Chat.Id, "Ответ верный!", replyMarkup: GetMainButtons());
            }
            else if (personMessage.Text != rightAnwser)
            #endregion
            else
            {
                await botClient.SendTextMessageAsync(personMessage.Chat.Id, "Ошибка 301: Не удалось распознать команду");
            }
            //Console.WriteLine(personMessage.Chat.Username); Имя пользователя
        }

        private static IReplyMarkup GetMainButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new List<KeyboardButton>{ new KeyboardButton { Text = "Играть 🚀" }, new KeyboardButton { Text = "Список лидеров 🏆" }  }
                }
            };
        }
        private static IReplyMarkup GetPlayButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new List<KeyboardButton>{ new KeyboardButton { Text = "Очень легко" }, new KeyboardButton { Text = "Легко" }, { new KeyboardButton { Text = "Легко+" }  }, {new KeyboardButton { Text = "Нормально" } } },
                    new List<KeyboardButton>{ new KeyboardButton { Text = "Нормально+" }, new KeyboardButton { Text = "Сложно" }, new KeyboardButton { Text = "Сложно+" }, new KeyboardButton { Text = "Супер сложно"} }
                }
            };
        }
    }
    public class AnwserMode
    {
        public void Anwser(string currentAnw, string example, int level, TelegramBotClient botClient, Telegram.Bot.Types.Message personMessage)
        {
            if (currentAnw == Calc.GetAnwserForExample(example, level))
            {

            }
        }
    }
    public class Calc
    {
        public static string GetAnwserForExample(string example, int level)
        {
            string charOfLevel = GetCharByLevel(level);
            example.Replace($" {charOfLevel} ", ".");
            var newExample = example.Split('.');
            var newExample1 = Convert.ToInt32(newExample[0]);
            var newExample2 = Convert.ToInt32(newExample[1]);
            if (charOfLevel == "+")
            {
                return (newExample1 + newExample2).ToString();
            }
            else if (charOfLevel == "-")
            {
                return (newExample1 - newExample2).ToString();
            }
            else if (charOfLevel == "•")
            {
                return (newExample1 * newExample2).ToString();
            }
            else
            {
                return (newExample1 / newExample2).ToString();
            }
        }
        public static string GetCharByLevel(int level)
        {
            if (level == 1 || level == 3 || level == 6)
            {
                return "+";
            }
            else if (level == 2)
            {
                return "-";
            }
            else if (level == 4 || level == 7)
            {
                return "•";
            }
            else return ":";
        }
    }
    public class Generator
    {
        public static int randomNumber(int from, int to)
        {
            var random = new Random();
            return random.Next(from, to);
        }

        public static string randomExample(int level)
        {
            #region levels
            if (level == 1)
            {
                return $"{randomNumber(10, 199)} + {randomNumber(10, 199)}";
            }
            else if (level == 2)
            {
                return $"{randomNumber(10, 199)} - {randomNumber(10, 199)}";
            }
            else if (level == 3)
            {
                return $"{randomNumber(10, 499)} + {randomNumber(10, 499)}";
            }
            else if (level == 4)
            {
                return $"{randomNumber(10, 49)} • {randomNumber(10, 49)}";
            }
            else if (level == 5)
            {
                int updatedRandomDivide1 = randomNumber(10, 29);
                int updatedRandomDivide2 = randomNumber(10, 29);
                if (updatedRandomDivide2 > updatedRandomDivide1)
                {
                    updatedRandomDivide1 = randomNumber(10, 29);
                    updatedRandomDivide2 = randomNumber(10, 29);
                    return $"{updatedRandomDivide1} : {updatedRandomDivide2}";
                }
                else
                {
                    return $"{updatedRandomDivide1} : {updatedRandomDivide2}";
                }
                string updatedExample = $"{updatedRandomDivide1} : {updatedRandomDivide2}";
                string temp = $"{randomNumber(10, 29)} : {randomNumber(10, 29)}";

            }
            else if (level == 6)
            {
                return $"{randomNumber(10, 1599)} + {randomNumber(10, 1599)}";
            }
            else if (level == 7)
            {
                return $"{randomNumber(10, 299)} • {randomNumber(10, 299)}";
            }
            else if (level == 8)
            {
                int updatedRandomDivide1 = randomNumber(10, 1999999);
                int updatedRandomDivide2 = randomNumber(10, 1999999);
                if (updatedRandomDivide2 > updatedRandomDivide1)
                {
                    updatedRandomDivide1 = randomNumber(10, 1999999);
                    updatedRandomDivide2 = randomNumber(10, 1999999);
                    return $"{updatedRandomDivide1} : {updatedRandomDivide2}";
                }
                else
                {
                    return $"{updatedRandomDivide1} : {updatedRandomDivide2}";
                }
            }
            #endregion 
            else
            {
                return "Ошибка 201: Не удалось сгенерировать пример, уровень не существует";
            }
        }
    }
}
